
package quizz;

import java.util.Scanner;


/**
 *          PROYECTO DEL QUIZZ
 * 
 * @author DUARTE HERNANDEZ EDUARDO IVAN
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE 
 * @since 20/7/2020
 */



public class Quizz {

   
    public static void main(String[] args) {
        // TODO code application logic here
      
        
        int valorpreguntas=2;
        int puntuaje=0;
      
      
      
      String pregunt1="¿Cuál es la sintaxis correcta para generar -HELLO WORD- en java?";
      String respuesta1="a";

      String pregunt2="¿Cómo se inserta comentarios de una linea en java?";
      String respuesta2="b";
      
      String pregunt3="¿Qué tipo de datos se utiliza para crear una variable que almacena texto?";
      String respuesta3="c";
      
      String pregunt4="¿Cómo se crea una variable numerica que asigne el valor 5?";
      String respuesta4="a";
      
      String pregunt5="¿Qué operador se utiliza para comparar dos valores?";
      String respuesta5="b";
      
      String pregunt6="¿Para declarar un arreglo, la variable se define como tipo?";
      String respuesta6="c";
    
      String pregunt7="¿Cuál es la sentancia correcta para crear un objeto llamado myObj de MyClass?";
      String respuesta7="b";

      String pregunt8="¿Cuál es el operador que se utiliza para multiplicar numeros?";
      String respuesta8="c";
     
      String pregunt9="¿Cómo se inicia escribiendo un ciclo while en java?";
      String respuesta9="a";
      
      String pregunt10="¿Cómo se inicia escribiendo un ciclo while en java?";
      String respuesta10="b";
      
      String pregunt11="¿Cuál es la instrucción que se utiliza para regresar un valor en un metodo?";
      String respuesta11="c";
     
      String pregunt12="¿Cómo se inserta comentarios para integrar al diccionario de datos de JavaDocs?";
      String respuesta12="a";
     
      
      Scanner preguntas=new Scanner(System.in);
      System.out.println("total de preguntas: 12" );
      System.out.println("valor de cadaa pregunta: 2" );
      
       System.out.println("1.-primera pregunta: "+pregunt1);
      System.out.println("a.- System.out.println (Hello Word)");
      System.out.println("b.- echo(Hello Word)" );
      System.out.println("c.- print(Hello Word)" );
      System.out.println("escribe tu respuesta" );
      String r1=preguntas.nextLine();
      
     
      if(r1.equalsIgnoreCase(respuesta1)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta1);
      }
      
        System.out.println("2.-segunda pregunta: "+pregunt2);
      System.out.println("a.- /*This is comment" );
      System.out.println("b.- //Thid is Comment" );
      System.out.println("c.- #This is Comment" );
      System.out.println("escribe tu respuesta" );
      String r2=preguntas.nextLine();
      
      if(r2.equalsIgnoreCase(respuesta2)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta2);
      }
      
        System.out.println("3.-tercera pregunta: "+pregunt3);
      System.out.println("a.- myString" );
      System.out.println("b.- strng" );
      System.out.println("c.- String" );
      System.out.println("escribe tu respuesta" );
      String r3=preguntas.nextLine();
      
      if(r3.equalsIgnoreCase(respuesta3)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta3);
      }
      
        System.out.println("4.-cuarta pregunta: "+pregunt4);
      System.out.println("a.- int x=5;" );
      System.out.println("b.- num x  =5" );
      System.out.println("c.- x =5;" );
      System.out.println("d.- float x =5;" );
      System.out.println("escribe tu respuesta" );
      String r4=preguntas.nextLine();
      
      if(r4.equalsIgnoreCase(respuesta4)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta4);
      }
      
        System.out.println("5.-quinta pregunta: "+pregunt5);
      System.out.println("a.- ><" );
      System.out.println("b.- ==" );
      System.out.println("c.- <>" );
      System.out.println("d.- =" );
      System.out.println("escribe tu respuesta" );
      String r5=preguntas.nextLine();
      
      if(r5.equalsIgnoreCase(respuesta5)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta5);
      }
      
        System.out.println("6.-sexta pregunta: "+pregunt6);
      System.out.println("a.- {}" );
      System.out.println("b.-´[]" );
      System.out.println("c.- ()" );
      System.out.println("escribe tu respuesta" );
      String r6=preguntas.nextLine();
      
      if(r6.equalsIgnoreCase(respuesta6)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta6);
      }
      
        System.out.println("7.-septima pregunta: "+pregunt7);
      System.out.println("a.- class MyClass = new myObj()" );
      System.out.println("b.- MyClass myObj = new MyClass()" );
      System.out.println("c.- new myObj()" );
      System.out.println("escribe tu respuesta" );
      String r7=preguntas.nextLine();
      
      if(r7.equalsIgnoreCase(respuesta7)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta7);
      }
      
        System.out.println("8.-octava pregunta: "+pregunt8);
      System.out.println("a.- %" );
      System.out.println("b.- x" );
      System.out.println("c.- *" );
      System.out.println("escribe tu respuesta" );
      String r8=preguntas.nextLine();
      
      if(r8.equalsIgnoreCase(respuesta8)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta8);
      }
      
        System.out.println("9.-novena pregunta: "+pregunt9);
      System.out.println("a.- if (x > y)" );
      System.out.println("b.- if x > y" );
      System.out.println("c.- if x > y then;" );
      System.out.println("escribe tu respuesta" );
      String r9=preguntas.nextLine();
      
      if(r9.equalsIgnoreCase(respuesta9)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta9);
      }
      
        System.out.println("10.-decima pregunta: "+pregunt10);
      System.out.println("a.- while x > y:" );
      System.out.println("b.- while x > y" );
      System.out.println("c.- while x > y{");
      System.out.println("escribe tu respuesta" );
      String r10=preguntas.nextLine();
      
      if(r10.equalsIgnoreCase(respuesta10)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta10);
      }
      
        System.out.println("11.-onceava pregunta: "+pregunt11);
      System.out.println("a.- break" );
      System.out.println("b.- get" );
      System.out.println("c.- return" );
      System.out.println("escribe tu respuesta" );
      String r11=preguntas.nextLine();
      
      if(r11.equalsIgnoreCase(respuesta11)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta11);
      }
      
        System.out.println("12.-doceavaa pregunta: "+pregunt12);
      System.out.println("a.- /**This is comment" );
      System.out.println("b.- /*This is comment" );
      System.out.println("c.- */This is comment" );
      System.out.println("escribe tu respuesta" );
      String r12=preguntas.nextLine();
      
      if(r12.equalsIgnoreCase(respuesta12)){
          puntuaje=puntuaje+1;
          System.out.println("Respuesta Correcta");
      }else{
          System.out.println("Respuesta Incorrecta, Respuesta correcta es: "+ respuesta12);
      }
      
      
      
      System.out.println("Tu Puntuaje es: "+valorpreguntas*puntuaje);
      
    }
    
}
