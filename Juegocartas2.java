package JUEGOCARTAS2;



public class Juegocartas2 {
    public static final String ANSI_BLACK= "\u001B[30m";
    public static final String ANSI_RED= "\u001B[31m";
    public static final String ANSI_GREEN= "\u001B[32m";
    public static final String ANSI_YELLOW= "\u001B[33m";
    public static final String ANSI_BLUE= "\u001B[34m";
    public static final String ANSI_PURPLE= "\u001B[35m";
    public static final String ANSI_CYAN= "\u001B[36m";
    public static final String ANSI_WHITE= "\u001B[37m";
    



    public static void main(String[] args) {
        
        
        
        Baraja b = new Baraja();
        
        System.out.println("Hay "+b.cartasDisponible()+" cartas disponibles");
       
        b.siguienteCarta();
        
        b.darCartas(5);
        
        System.out.println("Hay "+b.cartasDisponible()+" cartas disponibles");
       
        System.out.println("Cartas sacadas de momento");
        
        b.cartasMonton();
        
        
        b.barajar();
       
        Carta[] c = b.darCartas(5);
        
        System.out.println("Cartas sacadas despues de barajar ");
        for(int i=0;i<c.length;i++){
            System.out.println(c[i]);
        }
        
    }
    
    public static String paloCarta(int valor){
   
        String Color;
        switch(valor){
            case 1:
                Color= ANSI_PURPLE+ "CORAAON" +ANSI_BLACK;
                break;
            case 2:
                Color= ANSI_PURPLE+ "TREVOL" +ANSI_BLACK;
                break;
            case 3:
                Color= ANSI_PURPLE+ "DIAMANTES" +ANSI_BLACK;
                break;
            case 4:
                Color= ANSI_PURPLE+ "PICAS" +ANSI_BLACK;
                break;
            default:
                Color = valor + "";
                break;
            
        }
            return Color;
        }
    
}
